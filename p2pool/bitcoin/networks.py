import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc
from operator import *


@defer.inlineCallbacks
def check_genesis_block(bitcoind, genesis_block_hash):
    try:
        yield bitcoind.rpc_getblock(genesis_block_hash)
    except jsonrpc.Error_for_code(-5):
        defer.returnValue(False)
    else:
        defer.returnValue(True)

nets = dict(

    limecoin=math.Object(
        P2P_PREFIX='fbc0b6db'.decode('hex'),
        P2P_PORT=9030,
        ADDRESS_VERSION=19,
        RPC_PORT=9031,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'limecoinaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),                           
        SUBSIDY_FUNC=lambda nBits, height: 100*100000000,
        BLOCKHASH_FUNC=lambda data: pack.IntType(256).unpack(__import__('xcoin_hash').getPoWHash(data)),
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('xcoin_hash').getPoWHash(data)),
        BLOCK_PERIOD=600, # s
        SYMBOL='LIM',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'Limecoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/Limecoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.limecoin'), 'limecoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='http://limecoin.coinexplorers.com/block/',
        ADDRESS_EXPLORER_URL_PREFIX='http://limecoin.coinexplorers.com/address/',
        TX_EXPLORER_URL_PREFIX='http://limecoin.coinexplorers.com/tx/',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**20 - 1), 
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
    ),

)
for net_name, net in nets.iteritems():
    net.NAME = net_name
