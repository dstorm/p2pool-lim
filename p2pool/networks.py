from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(

    limecoin=math.Object(
        PARENT=networks.nets['limecoin'],
        SHARE_PERIOD=15, # seconds
        NEW_SHARE_PERIOD=15, # seconds
        CHAIN_LENGTH=6*60*60//10, # shares
        REAL_CHAIN_LENGTH=6*60*60//10, # shares
        TARGET_LOOKBEHIND=20, # shares  //with that the pools share diff is adjusting faster, important if huge hashing power comes to the pool
        SPREAD=6, # blocks
        NEW_SPREAD=6, # blocks
        IDENTIFIER='3E5E7753FFB20702'.decode('hex'),
        PREFIX='75D1478B4AD18B25'.decode('hex'),
        P2P_PORT=8030,
        MIN_TARGET=4,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=8031,
        BOOTSTRAP_ADDRS='omargpools.ca lim.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-lim',
        VERSION_CHECK=lambda v: True,
    ),

)
for net_name, net in nets.iteritems():
    net.NAME = net_name
